package me.bytropical.mutevote.vote;

import lombok.Getter;
import me.bytropical.mutevote.VotePlugin;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class Vote {

    @Getter final ProxiedPlayer vote_creater, targetuser;
    @Getter private final List<ProxiedPlayer> yes = new ArrayList<>(), no = new ArrayList<>();
    @Getter private boolean isactive;
    @Getter private String reason;

    public Vote(final ProxiedPlayer voter, final ProxiedPlayer target, final String reason) {
        this.vote_creater = voter;
        this.targetuser = target;
        this.reason = reason;

        if(voterHasActiveVote()) {
            voter.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Du hast bereits eine Abstimmmung am laufen")));
            return;
        }

        if(targetHasActiveVote()) {
            voter.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Du hat bereits gevoted")));
            return;
        }

        this.isactive = true;

        VotePlugin.getInstance().getVoteManager().getActivevotes().put(target.getUniqueId(), this);
        VotePlugin.getInstance().getVoteManager().getCreator().put(voter.getUniqueId(), this);

        TextComponent voteYes = new TextComponent(VotePlugin.getInstance().getConfigString("Stimme mit Yes ab").replaceAll("%player%", target.getName()));
        voteYes.setBold(true);
        voteYes.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(VotePlugin.getInstance().getConfigString("Stimme mit Yes ab").replaceAll("%player%", target.getName())).create()));
        voteYes.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mvote " + target.getName() + " yes"));

        TextComponent voteNo = new TextComponent(VotePlugin.getInstance().getConfigString("Stimme mit No ab").replaceAll("%player%", target.getName()));
        voteNo.setBold(true);
        voteNo.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(VotePlugin.getInstance().getConfigString("Stimme mit Yes ab").replaceAll("%player%", target.getName())).create()));
        voteNo.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mvote " + target.getName() + " no"));

        TextComponent space = new TextComponent( "\n");
        TextComponent Nachricht = new TextComponent();
        Nachricht.addExtra(voteYes);
        Nachricht.addExtra(space);
        Nachricht.addExtra(voteNo);

        final StringBuilder newVote = new StringBuilder().append(VotePlugin.getInstance().getPrefix()).append(VotePlugin.getInstance().getConfigString("Es wurde eine neue Abstimmung gestarted")).append("\n").
                append(VotePlugin.getInstance().getPrefix()).append(VotePlugin.getInstance().getConfigString("Abstimmung gegen").replaceAll("%player%", target.getName())).append("\n")
                .append(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Grund").replaceAll("%grund%", reason)).append("\n");

        VotePlugin.getInstance().getProxy().getPlayers().forEach(all -> {
            all.sendMessage(new TextComponent(newVote.toString()));
            all.sendMessage(Nachricht);
        });

        ProxyServer.getInstance().getScheduler().schedule(VotePlugin.getInstance(), this::endVote, 10, TimeUnit.SECONDS);
    }

    protected boolean targetHasActiveVote() {
        if(VotePlugin.getInstance().getVoteManager().getActivevotes().containsKey(targetuser.getUniqueId())) return true;
        return false;
    }

    protected boolean voterHasActiveVote() {
        if(VotePlugin.getInstance().getVoteManager().getCreator().containsKey(vote_creater.getUniqueId())) return true;

        return false;
    }

    public boolean isVoteActive() {
        if(isactive) return true;
        return false;
    }

    protected boolean hasVoted(final ProxiedPlayer voter) {
        if(getYes().contains(voter) || getNo().contains(voter)) return true;
        return false;
    }

    public void vote(final ProxiedPlayer voter, final VoteType type) {
        if(!isVoteActive()) {
            voter.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Dieser Vote existiert nicht")));
            return;
        }

        if(hasVoted(voter)) {
            voter.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Du hat bereits gevoted")));
            return;
        }

        if(type == VoteType.YES) {
            this.getYes().add(voter);
            voter.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Für den Vote gestimmt")));
        } else {
            this.getNo().add(voter);
            voter.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Gegen den Vote gestimmt")));
        }
    }


    protected void endVote() {
        VotePlugin.getInstance().getLogger().info("[OK] Ending Vote....");
        if(!isVoteActive()) return;

        int yes , no;
        yes = getYes().size();
        no = getNo().size();

        isactive = false;
        VotePlugin.getInstance().getVoteManager().getActivevotes().remove(targetuser.getUniqueId());
        VotePlugin.getInstance().getVoteManager().getCreator().remove(vote_creater.getUniqueId());
        getNo().clear();
        getYes().clear();

         final StringBuilder end = new StringBuilder()
                 .append(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Abstimmung hat geendet").replaceAll("%player%", targetuser.getName())).append("\n");

        if(yes == no || yes < no) {
            end.append(VotePlugin.getInstance().getPrefix()).append(VotePlugin.getInstance().getConfigString("Spieler wurde nicht gemuted")).append("\n");

        } else if(yes > no) {
            end.append(VotePlugin.getInstance().getPrefix()).append(VotePlugin.getInstance().getConfigString("Spieler wurde gemuted")).append("\n");

            try {
                VotePlugin.getInstance().getMuteManager().mutePlayer(vote_creater, targetuser, reason);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        end.append(VotePlugin.getInstance().getPrefix()).append(VotePlugin.getInstance().getConfigString("Stimmen für den Mute").replaceAll("%stimmen%", String.valueOf(yes))).append("\n");
        end.append(VotePlugin.getInstance().getPrefix()).append(VotePlugin.getInstance().getConfigString("Stimmen gegen den Mute").replaceAll("%stimmen%", String.valueOf(no)));

        ProxyServer.getInstance().getPlayers().forEach(all -> {
            all.sendMessage(new TextComponent(end.toString()));
        });
    }

}
