package me.bytropical.mutevote.vote;

import lombok.Getter;
import me.bytropical.mutevote.VotePlugin;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class VoteManager {

    @Getter private final HashMap<UUID, Vote> activevotes = new HashMap<UUID, Vote>(), creator = new HashMap<>();

    public boolean targetHasActiveVote(final ProxiedPlayer target_user) {
        if( activevotes.containsKey(target_user.getUniqueId())) return true;
        return false;
    }

    public Vote getVoteByCreator(final ProxiedPlayer creator) {
         return activevotes.getOrDefault(creator.getUniqueId(), null);
    }

    public Vote getVoteByTarget(final ProxiedPlayer target) {

        Vote[] vo = new Vote[1];
        vo[0] = null;

        activevotes.values().forEach(v -> {
            if(v.getTargetuser().getUniqueId() == target.getUniqueId()) {
                vo[0] = v;
            }
        });

        return vo[0];
    }

}
