package me.bytropical.mutevote;

import lombok.Getter;
import me.bytropical.mutevote.commands.CreatevoteCommand;
import me.bytropical.mutevote.commands.MVoteCommand;
import me.bytropical.mutevote.commands.UnmvCommand;
import me.bytropical.mutevote.database.MySQLManager;
import me.bytropical.mutevote.listener.PlayerChatListener;
import me.bytropical.mutevote.listener.PlayerDisconnectListener;
import me.bytropical.mutevote.listener.ServerConnectListener;
import me.bytropical.mutevote.mute.MuteManager;
import me.bytropical.mutevote.utils.Cache;
import me.bytropical.mutevote.utils.Messages;
import me.bytropical.mutevote.vote.VoteManager;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class VotePlugin extends Plugin {

    @Getter private static VotePlugin instance;
    @Getter private MySQLManager mysql;
    @Getter private VoteManager voteManager;
    @Getter private MuteManager muteManager;
    @Getter private Cache cache;
    @Getter private Configuration conf;
    @Getter private String prefix;
    @Getter private File config;
    @Getter private boolean fileExists;

    private String host, user, psw, db;
    private int port;

    @Override
    public void onEnable() {
        this.instance = this;

        try {

            loadAndCreateConfig();
             connectToDatabase();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.voteManager = new VoteManager();
        this.muteManager = new MuteManager();
        this.cache = new Cache();

        this.getProxy().getPluginManager().registerCommand(this, new CreatevoteCommand("createmv"));
        this.getProxy().getPluginManager().registerCommand(this, new MVoteCommand("mvote"));
        this.getProxy().getPluginManager().registerCommand(this, new UnmvCommand("Unmv"));

        this.getProxy().getPluginManager().registerListener(this, new PlayerChatListener());
        this.getProxy().getPluginManager().registerListener(this, new ServerConnectListener());
        this.getProxy().getPluginManager().registerListener(this, new PlayerDisconnectListener());

        this.prefix = conf.getString("Prefix").replaceAll("&","§") + " ";
        getLogger().info("[Ok] Mutevote Plugin could load all Resources...");
    }

    @Override
    public void onDisable() {
    }

    private void connectToDatabase() throws SQLException {
        this.mysql = new MySQLManager(host,user, psw,db, port);
        getLogger().info("[Ok] Connected to MariaDB...");
    }

    private void loadAndCreateConfig() throws IOException {
        if(!this.getDataFolder().exists()) {
            this.getDataFolder().mkdir();
        }

        this.config = new File(getDataFolder(), "config.yml");
        if(!config.exists()) {
            config.createNewFile();
            this.fileExists = false;
        } else {
            this.fileExists = true;
        }

        conf = ConfigurationProvider.getProvider(YamlConfiguration.class).load(config);
        if(!fileExists) {
           new Messages(conf);

            ConfigurationProvider.getProvider(YamlConfiguration.class).save(conf, config);
        }

        this.host = conf.getString("host");
        this.user = conf.getString("user");
        this.psw = conf.getString("password");
        this.db = conf.getString("db");
        this.port = conf.getInt("port");

     getLogger().info("[Ok] Created / loaded Config...");

    }

    public String getConfigString(final String conf) {
        if(!this.conf.contains(conf)) return "§CCOULD NOT FOUND IN CONFIG!";
        return this.getConf().getString(conf).replaceAll("&","§");
    }

    public Long getConfigLong(final String conf) {
        if(!this.conf.contains(conf)) return 1L;
        return this.getConf().getLong(conf);
    }

}
