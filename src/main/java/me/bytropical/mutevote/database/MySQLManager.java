package me.bytropical.mutevote.database;

import lombok.Getter;
import me.bytropical.mutevote.VotePlugin;

import java.sql.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class MySQLManager {
    @Getter private ExecutorService executor = Executors.newFixedThreadPool(4);
    @Getter private Connection connection;

    protected final String host, user, password, database;
    protected final int port;

    public MySQLManager(final String host, final String user, final String password, final String database, final int port) {
        this.host = host;
        this.user = user;
        this.password = password;
        this.database = database;
        this.port = port;

        executor.execute(() -> {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database, this.user, this.password);

                update("CREATE TABLE IF NOT EXISTS mutedPlayer(UUID varchar(40), name varchar(16), reason varchar(30), time long)");
                update("CREATE TABLE IF NOT EXISTS mutedPlayerList(name varchar(16), reason varchar(30))");

            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    public boolean isConnected() throws SQLException {
        return getConnection().isClosed();
    }

    public void update(String qry) {
        executor.execute(() -> {
            Statement st;
            try {
                st = connection.createStatement();
                st.executeUpdate(qry);
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public ResultSet query(String qry) {
        ResultSet rs = null;

        try {
            Statement st = getConnection().createStatement();
            rs = st.executeQuery(qry);
        } catch (SQLException e) {
            System.err.println(e);
        }
        return rs;
    }

}