package me.bytropical.mutevote.listener;

import me.bytropical.mutevote.VotePlugin;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.sql.SQLException;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class ServerConnectListener implements Listener {

    @EventHandler
    public void onPlayerJoin(final ServerConnectEvent event) {
        final ProxiedPlayer player = event.getPlayer();

        try {
            if(VotePlugin.getInstance().getMuteManager().isPlayerMuted(player.getUniqueId())) VotePlugin.getInstance().getCache().getCache().put(player.getUniqueId(), player.getName());
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
