package me.bytropical.mutevote.listener;

import me.bytropical.mutevote.VotePlugin;
import me.bytropical.mutevote.vote.VoteManager;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.sql.SQLException;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class PlayerChatListener implements Listener {

    @EventHandler
    public void on(final ChatEvent event) {
        final ProxiedPlayer player = (ProxiedPlayer) event.getSender();

        try {

            if(event.getMessage().startsWith("/")) return;
            if(VotePlugin.getInstance().getMuteManager().isPlayerMuted(player.getUniqueId())) {
                event.setCancelled(true);
                player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Du bist gemuted")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
