package me.bytropical.mutevote.listener;

import me.bytropical.mutevote.VotePlugin;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class PlayerDisconnectListener implements Listener {

    @EventHandler
    public void onPlayerDisconnect(final PlayerDisconnectEvent event) {
        final ProxiedPlayer player = event.getPlayer();

        if(VotePlugin.getInstance().getCache().isPlayerInCache(player.getUniqueId())) VotePlugin.getInstance().getCache().getCache().asMap().remove(player.getUniqueId());
    }
}
