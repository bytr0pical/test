package me.bytropical.mutevote.mute;

import me.bytropical.mutevote.VotePlugin;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class MuteManager {


    public void mutePlayer(final ProxiedPlayer player, final ProxiedPlayer target, final String reason) throws SQLException {
        if(isPlayerMuted(target.getUniqueId())) {
            player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("player_is_muted")));
            return;
        }
        final String[] time = VotePlugin.getInstance().getConfigString("time").split(" ");
        long millis, current, minutes, hours, end;

        current = System.currentTimeMillis();
        millis = Long.valueOf(time[4]) * 1000;
        minutes = Long.valueOf(time[2]) * 1000 * 60;
        hours = Long.valueOf(time[0])  * 1000 * 60 * 60;

        end = current + millis + minutes + hours;

        VotePlugin.getInstance().getMysql().update("INSERT INTO mutedPlayer(UUID,name,reason,time) VALUES ('" + target.getUniqueId().toString() + "','" + target.getName() + "','" + reason + "','" + end + "')");
        if(target == null) return;
        if(VotePlugin.getInstance().getCache().isPlayerInCache(target.getUniqueId())) return;
        VotePlugin.getInstance().getCache().getCache().put(target.getUniqueId(), target.getName());
    }

    public void unmutePlayer(final ProxiedPlayer player, final ProxiedPlayer target) {
        try {
            if(!isPlayerMuted(target.getUniqueId())) {
                player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Der Spieler wurde nicht gemuted")));
                return;
            }

            VotePlugin.getInstance().getMysql().update("DELETE FROM mutedPlayer WHERE UUID='" + target.getUniqueId() + "'");

            player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Du hast den Spieler gemuted")));

            if(target == null) return;
            target.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Du wurdest gemuted")));
            if(!VotePlugin.getInstance().getCache().isPlayerInCache(target.getUniqueId())) return;
            VotePlugin.getInstance().getCache().getCache().asMap().remove(target.getUniqueId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Long getEnd(final UUID targetUUID) throws SQLException {
        PreparedStatement st = VotePlugin.getInstance().getMysql().getConnection().prepareStatement("SELECT * FROM mutedPlayer WHERE UUID='" + targetUUID + "'");
        final ResultSet rs =   st.executeQuery();
        while (rs.next()) return rs.getLong("time");
        st.close();
        return System.currentTimeMillis() + 1;
    }

    public boolean isPlayerMuted(final UUID targetUUID) throws SQLException {

        if(System.currentTimeMillis() > getEnd(targetUUID)) {
            VotePlugin.getInstance().getMysql().update("DELETE FROM mutedPlayer WHERE UUID='" + targetUUID + "'");
            ProxyServer.getInstance().getPlayer(targetUUID).sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Du wurdest gemuted")));
            if(!VotePlugin.getInstance().getCache().isPlayerInCache(targetUUID)) return false;
            VotePlugin.getInstance().getCache().getCache().asMap().remove(targetUUID);
            return false;
        }

            if(VotePlugin.getInstance().getCache().isPlayerInCache(targetUUID)) return true;

        PreparedStatement st = VotePlugin.getInstance().getMysql().getConnection().prepareStatement("SELECT * FROM mutedPlayer WHERE UUID='" + targetUUID + "'");
        final ResultSet rs =   st.executeQuery();
        while (rs.next()) return true;
        st.close();
        return false;
    }
}
