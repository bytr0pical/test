package me.bytropical.mutevote.utils;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.Getter;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class Cache {

    @Getter private final LoadingCache<UUID, String> cache = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .build(
           new CacheLoader<UUID, String>() {
               @Override
               public String load(UUID uuid) throws Exception {
                   return null;
               }
            });

    public boolean isPlayerInCache(final UUID uuid) {
        if(cache.asMap().containsKey(uuid)) return true;
        return false;
    }

}
