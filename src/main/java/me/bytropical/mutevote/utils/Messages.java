package me.bytropical.mutevote.utils;

import me.bytropical.mutevote.VotePlugin;
import net.md_5.bungee.config.Configuration;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class Messages {

    public Messages(Configuration conf) {

        // MySQL Daten
        conf.set("host","localhost");
        conf.set("user","user");
        conf.set("password","password");
        conf.set("db","db");
        conf.set("port",3306);

        //Mute Time
        conf.set("time", "0 Stunden 0 Minuten 10 Sekunden");

        //Messages
        conf.set("Prefix","&2Mutevote &8>");
        conf.set("CreateVote Commandsyntax", "&7Nutze &2/createmv [Name] [Grund]");
        conf.set("Mvote Commandsyntax", "&7Nutze &2/mvote [target] yes | no");
        conf.set("Unmv Commandsyntax", "&7Nutze &2/unvm [Spieler]");
        conf.set("Spieler ist nicht mehr Online", "&cDer Spieler ist leider nicht mehr online!");
        conf.set("Der Spieler ist bereits gemuted", "&cDer Spieler ist bereits gemuted!");
        conf.set("Du bist gemuted", "&7Du bist derzeitig gemuted");
        conf.set("Gegen den Spieler läuft kein Vote", "&cGegen den Spieler läuft kein aktiver Vote!");
        conf.set("Nutze bitte yes oder no", "&7Nutze bitte &ayes &7oder &cno");
        conf.set("Du hast den Spieler gemuted", "&7Du hast den Spieler entmuted");
        conf.set("Du wurdest gemuted", "&7Du wurdest entmuted");
        conf.set("Du hast bereits eine Abstimmmung am laufen", "&cDu hast bereits eine Abstimmung am laufen!");
        conf.set("Der Spieler hat bereits einen Vote am laufen", "&cDer Spieler hat bereits ein aktiven Vote am laufen");
        conf.set("Du hat bereits gevoted", "&cDu hast bereits gevoted!");
        conf.set("Für den Vote gestimmt", "&7Du hast &2für&7 den Mute gestimmt!");
        conf.set("Gegen den Vote gestimmt", "&7Du hast &cgegen&7 den Mute gestimmt!");
        conf.set("Abstimmung hat geendet", "&7Die Abstimmung über&e %player% &7hat geendet!");
        conf.set("Spieler wurde nicht gemuted","&7Der Spieler wurde nicht &cgemuted&7!");
        conf.set("Spieler wurde gemuted","&7Der Spieler wurde &2gemuted&7!");
        conf.set("Stimmen für den Mute", "&7Stimmen für den Mute: &e %stimmen%");
        conf.set("Stimmen gegen den Mute", "&7Stimmen für gegen den Mute: &c %stimmen%  ");
        conf.set("Es wurde eine neue Abstimmung gestarted", "&7Es wurde eine neue Muteabstimmung gestarted!");
        conf.set("Direkt Vote Message", "&7Klicke auf die Nachricht um direkt zu Voten");
        conf.set("Abstimmung gegen", "&7Abstimmung gegen&7:&2 %player%");
        conf.set("Grund", "&7Grund:&e %grund%");
        conf.set("Stimme mit Yes ab", "&7Stimme mit &2/mvote %player% yes &7für den Vote ab");
        conf.set("Stimme mit No ab", "&7Stimme mit &c/mvote %player% no &7gegen den Vote ab");
        conf.set("Dieser Vote existiert nicht", "&cDieser Vote existiert nicht!");

    }

}
