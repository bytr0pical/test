package me.bytropical.mutevote.commands;

import me.bytropical.mutevote.VotePlugin;
import me.bytropical.mutevote.vote.Vote;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.SQLException;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class CreatevoteCommand extends Command {

    public CreatevoteCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (!(sender instanceof ProxiedPlayer)) return;

        final ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length < 2 || args.length > 8) {
            player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("CreateVote Commandsyntax")));
            return;
        }

        final ProxiedPlayer target = VotePlugin.getInstance().getProxy().getPlayer(args[0]);

        if (target == null) {
            player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Spieler ist nicht mehr Online")));
            return;
        }
        try {
            if(VotePlugin.getInstance().getMuteManager().isPlayerMuted(target.getUniqueId())) {
                player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Der Spieler ist bereits gemuted")));
                return;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        StringBuilder builder = new StringBuilder();
        for(int i = 1;i<args.length;i++){
            if(i == args.length){
                builder.append(args[i]);
            }else{
                builder.append(args[i]).append(" ");
            }
        }


        new Vote(player, target, builder.toString());

    }
}
