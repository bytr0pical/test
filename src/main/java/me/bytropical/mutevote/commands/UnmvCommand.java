package me.bytropical.mutevote.commands;

import me.bytropical.mutevote.VotePlugin;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class UnmvCommand extends Command {

    public UnmvCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if(!(sender instanceof ProxiedPlayer)) return;
        final ProxiedPlayer player = (ProxiedPlayer) sender;

        if(args.length != 1) {
            player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Unmv Commandsyntax")));
            return;
        }

        final ProxiedPlayer target = VotePlugin.getInstance().getProxy().getPlayer(args[0]);

        VotePlugin.getInstance().getMuteManager().unmutePlayer(player,target);
    }
}
