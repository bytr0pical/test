package me.bytropical.mutevote.commands;

import me.bytropical.mutevote.VotePlugin;
import me.bytropical.mutevote.vote.Vote;
import me.bytropical.mutevote.vote.VoteType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ByTropical
 * Version: 1.0
 */

public class MVoteCommand extends Command {


    public MVoteCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if(!(sender instanceof ProxiedPlayer)) return;
        final ProxiedPlayer player = (ProxiedPlayer) sender;

        if(args.length != 2) {
            player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() +  VotePlugin.getInstance().getConfigString("Mvote Commandsyntax")));
            return;
        }

        final ProxiedPlayer target = VotePlugin.getInstance().getProxy().getPlayer(args[0]);

        if(target == null) {
            player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Spieler ist nicht mehr Online")));
            return;
        }
        if(!VotePlugin.getInstance().getVoteManager().targetHasActiveVote(target)) {
            player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Dieser Vote existiert nicht")));
            return;
        }

        final String vote_type = args[1];

        if(!voteWords().contains(vote_type)) {
            player.sendMessage(new TextComponent(VotePlugin.getInstance().getPrefix() + VotePlugin.getInstance().getConfigString("Nutze bitte yes oder no")));
            return;
        }

        Vote vote = VotePlugin.getInstance().getVoteManager().getVoteByTarget(target);

        if(vote_type.equalsIgnoreCase("yes") || vote_type.equalsIgnoreCase("y") || vote_type.equalsIgnoreCase("ja")) {
            vote.vote(target, VoteType.YES);
        } else {
            vote.vote(target, VoteType.NO);
        }
    }

    private List<String> voteWords() {
        final List<String> list = new ArrayList<>();
        list.add("yes");
        list.add("y");
        list.add("ja");
        list.add("no");
        list.add("n");
        list.add("mute");
        list.add("neee");
        list.add("nein");

        return list;
    }
}
